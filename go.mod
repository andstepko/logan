module gitlab.com/distributed_lab/logan

go 1.12

require (
	github.com/go-errors/errors v0.0.0-20161205223245-8fa88b06e597
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.0.3
	golang.org/x/crypto v0.0.0-20170825220121-81e90905daef
	golang.org/x/sys v0.0.0-20170909063139-a5054c7c1385
)
